vagrant-ansible-ceph
============

Ansible playbook for Ceph!

Clone me:

```bash
git@gitlab.com:academm/vagrant-ansible-ceph.git
```

## What does it do?

deploys four VMs:

* Monitors:
  ceph-server-3
* OSDs:
  ceph-server-1
  ceph-server-2
  ceph-server-3
* Client:
  ceph-client

